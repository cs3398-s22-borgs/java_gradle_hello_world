package hello;

// on line change
//Andersons Assignment 9 edit

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


// Starting class (3-4)
// this is Ringtarih's comment

//nim21 assignmnet 9 test comment

public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty() 
   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }

   @Test
   @DisplayName("Test for Ted")
   public void testGreeterTed() 
   {
      g.setName("Ted");
      assertEquals(g.getName(),"Ted");
      assertEquals(g.sayHello(),"Hello Ted!");
   }

   @Test
   @DisplayName("Test for Name='Uyen'")
   public void testGreeterUyen() 
   {
      g.setName("Uyen");
      assertEquals(g.getName(),"Uyen");
      assertEquals(g.sayHello(),"Hello Uyen!");
   }
   @Test
   @DisplayName("Test for Ringtarih")
   public void testGreeterRingtarih() 
    //this is the test with displaying my name 

   {
      g.setName("Ringtarih");
      assertEquals(g.getName(),"Ringtarih");
      assertEquals(g.sayHello(),"Hello Ringtarih!");
   }

   //this is a test with assertFalse() function
   @Test
   @DisplayName("Test for False case")
   public void testAssertFalse() 
   {
      assertFalse(2 == 4);
   } 

   @Test
   @DisplayName("Test for true != false")
   public void testAssertFalseLogic(){
       assertFalse(true == false);
   } 
   
   @Test
   @DisplayName("Test for Name='World'")
   public void testGreeter() 
   {
      g.setName("World");
      assertEquals(g.getName(),"World");
      assertEquals(g.sayHello(),"Hello World!");
   }
   
   @Test
   @DisplayName("Test for Nick")
   public void testGreeterNick() 

   {
      g.setName("Nick");
      assertEquals(g.getName(),"Nick");
      assertEquals(g.sayHello(),"Hello Nick!");
   }

   @Test
   @DisplayName("Test for assertFalse with String message")
   public void testAssertFalseString()
   {
     assertFalse("hi" == "hello", "hi does not equal hello");
   }
    @Test
   @DisplayName("Test for NickA14")
   public void testGreeterNickA14() 

   {
      g.setName("NickA14");
      assertEquals(g.getName(),"NickA14");
      assertEquals(g.sayHello(),"Hello NickA14!");
   }
        

   @Test
   @DisplayName("Test for Anderson")
   public void testGreeterAnderson() 

   {
      g.setName("Anderson");
      assertEquals(g.getName(),"Anderson");
      assertEquals(g.sayHello(),"Hello Anderson!");
   }
   @Test
   @DisplayName("Test for AndersonA14")
   public void testGreeterAndersonA14() 

   {
      g.setName("AndersonA14");
      assertEquals(g.getName(),"AndersonA14");
      assertEquals(g.sayHello(),"Hello AndersonA14!");
   }

   @Test
   @DisplayName("Test for assertFail")
   public void assertFail() 
   {
      assertFalse('a' == 'b');
      
   }

   @Test
   @DisplayName("Test for utn2_A14")
   public void testGreeterutn2_A14() 
   {
      g.setName("utn2_A14");
      assertEquals(g.getName(),"utn2_A14");
      assertEquals(g.sayHello(),"Hello utn2_A14!");
   }
    @Test
   @DisplayName("Test for rkt24A14")
   public void testGreeterrkt24A14() 

   {
      g.setName("rkt24A14");
      assertEquals(g.getName(),"rkt24A14");
      assertEquals(g.sayHello(),"Hello rkt24A14!");
   }

}
